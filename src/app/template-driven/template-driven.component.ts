import { Component } from '@angular/core';
import { Hero } from '../hero';
@Component({
  selector: 'app-template-driven',
  templateUrl: './template-driven.component.html',
  styleUrls: ['./template-driven.component.css'],
})
export class TemplateDrivenComponent {
  powers = ['Really Smart', 'Super Flexible', 'Super Hot', 'Weather Changer'];

  model = new Hero(18, 'Dr. IQ', this.powers[0], 'Chuck Overstreet');

  submitted = false;

  onSubmit() {
    // this.submitted = true;
    alert("thành công gởi đi rồi mloz");
  }
  newHero() {
    this.model = new Hero(42, '', '');
  }
  
}
